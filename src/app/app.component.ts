import { Component, OnInit,NgZone, HostListener  } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'testing';
  myItems = [
    {
      description : 'vddvdsvv',
      title: 'one',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'gewge',
      title: 'two',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'fwef',
      title: 'three',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'vwewv',
      title: 'four',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'fwqfwq',
      title: 'five',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'six',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'seven',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'eight',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'nine',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'ten',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'eleven',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'twelve',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'thirteen',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'fourteen',
      link: 'https://picsum.photos/500/300/?image=10'
    },
    {
      description : 'ewfefwe',
      title: 'fifteen',
      link: 'https://picsum.photos/500/300/?image=10'
    },
  ];

  iterable = [];
  constructor() { }

  ngOnInit() {
   this.addValuesIntoArray(0, 5);
  }

  addValuesIntoArray(start, upTo) {
    if(this.myItems.length > start && this.myItems.length > upTo) {
      let lastVal = upTo + 5;
      console.log(lastVal);
      
      if(this.myItems.length > lastVal) {
        for (let index = start; index < lastVal; index++) {
          const element = this.myItems[index];
          console.log(lastVal);
          this.iterable.push(element);
        }
      } else {
        for (let index = start; index < this.myItems.length; index++) {
          const element = this.myItems[index];
          console.log(lastVal);
          this.iterable.push(element);
        }
      }
    }

  }

@HostListener('scroll', ['$event'])
onScroll(event: any) {
    // visible height + pixel scrolled >= total height 
    if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
      console.log("End");
      this.addValuesIntoArray(this.iterable.length, this.iterable.length);
    }
}

}
